package shared

import (
	"fyne.io/fyne"
	"fyne.io/fyne/widget"
	"os"
)

var (
	CManager = &ComboManager{}
	PManager = &ProxyManager{}
	BManager = &BotManager{}
	BotCount int = 15
	MainWindow fyne.Window
	Running = false

	CheckProgress *widget.ProgressBar
	HitsList *widget.Box
	ProxyCounter *widget.Label
	ComboCounter *widget.Label
	ValidCombosCounter *widget.Label
	BadCombosCounter *widget.Label
	OutFile *os.File

)