package shared

import (
	"bufio"
	"errors"
	"os"
	"strings"
)

const (
	UNCHECKED = iota
	VALID
	BAD
)

type Combo struct {
	Username string
	Password string
	InUse bool
	Status int
	Identifier int
	Capture []string
}

func (c *Combo) AddCaptureStr (data string) {
	c.Capture = append(c.Capture, data)
}

func (c *Combo) AddCaptureInt (name string, data int) {
	//c.Capture[name] = strconv.Itoa(data)
}

func (c *Combo) ToString() string {
	a := c.Username + ":" + c.Password + " [ "
	for _,k := range c.Capture {
		a +=  k +", "
	}
	a += "]"
	return a
}

type ComboManager struct {
	ComboList []*Combo
}

func (cm *ComboManager) LoadFromFile (filename string) (int, error) {
	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		return 0, err
	}
	b := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		b++
		a := strings.Split(scanner.Text(), ":")
		c := &Combo{a[0], a[1], false, UNCHECKED, len(cm.ComboList), []string{}}
		cm.ComboList = append(cm.ComboList, c)
	}

	if err := scanner.Err(); err != nil {
		return 0, err
	}
	return b, nil
}

func (cm *ComboManager) GetNextCombo () (*Combo, error) {
	for _,k := range cm.ComboList {
		if k.InUse == false && k.Status == UNCHECKED {
			k.InUse = true
			return k, nil
		}
	}
	return nil, errors.New("out of combos")
}

func (cm *ComboManager) GetCombosByStatus(status int) ([]*Combo, error) {
	var a []*Combo
	for _,k := range cm.ComboList {
		if k.Status == status {
			a = append(a, k)
		}
	}
	if len(a) > 0 {
		return a,nil
	}
	return nil, errors.New("no combos with status")
}

func (cm *ComboManager) GetTotalCombos() int {
	return len(cm.ComboList)
}

func (cm *ComboManager) SetComboStatus(c *Combo, status int) {
	cm.ComboList[c.Identifier].Status = status
}

func (cm *ComboManager) ReleaseCombo(combo *Combo) {
	cm.ComboList[combo.Identifier].InUse = false
}

func (cm *ComboManager) GetComboCount(status int) int {
	a := 0
	for _,k := range cm.ComboList {
		if k.Status == status {
			a++
		}
	}
	return a
}

