package shared

import (
	"bufio"
	"github.com/pkg/errors"
	"os"
	"strings"
)

const (
	HTTP = iota
	SOCKS4
	SOCKS4A
	SOCKS5
)

type Proxy struct {
	Host string
	Port string
	IsOnline bool
	IsBusy bool
	Identifier int
	ProxyType int
}

func (p *Proxy) GetTypeStr() string {
	switch p.ProxyType {
	default:
		fallthrough
	case HTTP:
		return "HTTP"
	case SOCKS4:
		return "SOCKS4"
	case SOCKS4A:
		return "SOCKS4A"
	case SOCKS5:
		return "SOCKS5"
	}
}

type ProxyManager struct {
	ProxyList []*Proxy
}

func (pm *ProxyManager) LoadFromFile(filename string, proxyType int) (int, error) {
	file, err := os.Open(filename)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	b := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		b++
		a := strings.Split(scanner.Text(), ":")
		p := &Proxy{a[0], a[1], true, false, len(pm.ProxyList), proxyType}
		pm.ProxyList = append(pm.ProxyList, p)
	}

	if err := scanner.Err(); err != nil {
		return 0, err
	}
	return b, nil
}

func (pm *ProxyManager) GetNextProxy() (*Proxy, error) {
	for _,k := range pm.ProxyList {
		if k.IsBusy == false && k.IsOnline == true {
			k.IsBusy = true
			return k, nil
		}
	}
	return nil, errors.New("out of proxies")
}

func (pm *ProxyManager) SetProxyOnline(p *Proxy, online bool) {
	pm.ProxyList[p.Identifier].IsOnline = online
}

func (pm *ProxyManager) SetAllOnline() {
	for _,k := range pm.ProxyList {
		k.IsOnline = true
	}
}

func (pm *ProxyManager) ReleaseAll() {
	for _,k := range pm.ProxyList {
		k.IsBusy = false
	}
}

func (pm *ProxyManager) ReleaseProxy(p *Proxy) {
	pm.ProxyList[p.Identifier].IsBusy = false
}

func (pm *ProxyManager) GetProxyCount() int {
	a := 0
	for _,k := range pm.ProxyList {
		if k.IsOnline {
			a++
		}
	}
	return a
}