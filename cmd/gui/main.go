package main

import (
	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
	"hulu/cmd/gui/screens"
	"hulu/shared"
	"net/url"
	"os"
)

func welcomeScreen(a fyne.App) fyne.CanvasObject {
	logo := canvas.NewImageFromResource(shared.Logo)
	logo.SetMinSize(fyne.NewSize(128, 128))

	link, err := url.Parse("https://zertex.space/")
	if err != nil {
		fyne.LogError("Could not parse URL", err)
	}

	return widget.NewVBox(
		widget.NewLabelWithStyle("Hulu Checker, created by Zertex", fyne.TextAlignCenter, fyne.TextStyle{Bold: true}),
		layout.NewSpacer(),
		widget.NewHBox(layout.NewSpacer(), logo, layout.NewSpacer()),
		widget.NewHyperlinkWithStyle("zertex.space", link, fyne.TextAlignCenter, fyne.TextStyle{}),
		layout.NewSpacer(),

		widget.NewGroup("Theme",
			fyne.NewContainerWithLayout(layout.NewGridLayout(2),
				widget.NewButton("Dark", func() {
					a.Settings().SetTheme(theme.DarkTheme())
				}),
				widget.NewButton("Light", func() {
					a.Settings().SetTheme(theme.LightTheme())
				}),
			),
		),
	)
}

func main() {
	a := app.New()
	a.SetIcon(shared.Logo)

	var err error
	shared.OutFile, err = os.OpenFile("output.txt", os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		shared.OutFile, err = os.Create("output.txt")
		if err != nil {
			panic(err)
		}
	}

	defer shared.OutFile.Close()

	shared.MainWindow = a.NewWindow("Hulu Checker")
	shared.MainWindow.SetPadded(true)/*
	shared.MainWindow.SetMainMenu(fyne.NewMainMenu(fyne.NewMenu("File",
		fyne.NewMenuItem("New", func() {
			fmt.Println("Menu New") }),
		// a quit item will be appended to our first menu
	), fyne.NewMenu("Edit",
		fyne.NewMenuItem("Cut", func() { fmt.Println("Menu Cut") }),
		fyne.NewMenuItem("Copy", func() { fmt.Println("Menu Copy") }),
		fyne.NewMenuItem("Paste", func() { fmt.Println("Menu Paste") }),
	)))*/

	tabs := widget.NewTabContainer(
		widget.NewTabItemWithIcon("Welcome", theme.HomeIcon(), welcomeScreen(a)),
		widget.NewTabItemWithIcon("Checker", theme.CheckButtonIcon(), screens.Checker(a)),
		widget.NewTabItemWithIcon("Lists", theme.FolderIcon(), screens.ListsScreen(a)),
		)

	shared.MainWindow.SetContent(tabs)

	shared.MainWindow.ShowAndRun()
}