package screens

import (
	"fmt"
	"fyne.io/fyne"
	"fyne.io/fyne/dialog"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
	"hulu/shared"
)

func ListsScreen(a fyne.App) fyne.CanvasObject {
	pType := 0
	return widget.NewHBox(
		widget.NewVBox(
			widget.NewLabelWithStyle("Load Combos", fyne.TextAlignCenter, fyne.TextStyle{Bold:true}),
			layout.NewSpacer(),
			widget.NewButton("Load combos.txt", func() {
				c, err := shared.CManager.LoadFromFile("combos.txt")
				if err != nil {
					dialog.ShowError(err, shared.MainWindow)
				} else {
					dialog.ShowInformation("Success", fmt.Sprintf("Loaded %d combos", c), shared.MainWindow)
					shared.ComboCounter.SetText(fmt.Sprintf("Combos: %d", c))
				}
			}),
		),
		layout.NewSpacer(),
		widget.NewVBox(
			widget.NewLabelWithStyle("Load Proxies", fyne.TextAlignCenter, fyne.TextStyle{Bold:true}),
			layout.NewSpacer(),
			widget.NewSelect([]string{"HTTP", "SOCKS4", "SOCKS4a", "SOCKS5"}, func(s string) {
				switch s {
				case "HTTP":
					pType = 0
				case "SOCKS4":
					pType = 1
				case "SOCkS4a":
					pType = 2
				case "SOCKS5":
					pType = 3
				}
			}),
			widget.NewButton("Load proxies.txt", func() {
				c, err := shared.PManager.LoadFromFile("proxies.txt", pType)
				if err != nil {
					dialog.ShowError(err, shared.MainWindow)
				} else {
					dialog.ShowInformation("Success", fmt.Sprintf("Loaded %d proxies", c), shared.MainWindow)
					shared.ProxyCounter.SetText(fmt.Sprintf("Proxies: %d", c))
				}
			}),
		),
	)
}
