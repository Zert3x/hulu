package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("not enough args")
		return
	}
	fmt.Println("Loading", os.Args[1])
	raw, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	base := "var logo = &fyne.StaticResource{\nStaticName: \""+os.Args[1]+"\",\nStaticContent: []byte{"
	fmt.Println("Processing...")
	for _,k := range raw {
		base += fmt.Sprintf("%d, ", k)
	}
	base += "}"
	fmt.Println("Writing to logo.go")
	err = ioutil.WriteFile("../../shared/logo.go", []byte(base), 0644)
	if err != nil {
		fmt.Println(err.Error())
	}
}
